#!/bin/sh

export TASK_DIR=$(cd `dirname $0`/..;pwd)

JAR_FILE=$1

[[ ! -f ${JAR_FILE} ]] && echo "Jar File Not Found!" && exit 1

if ( ps -ef | grep -v grep | grep -v $$ | grep -q $JAR_FILE ); then
    echo "$JAR_FILE process already running." && exit 2
    # jps | grep -vi jps | grep $JAR_FILE | awk '{print $1}' | xargs kill
fi

nohup java -Xmx4G -Xss4G -XX:+UseG1GC -jar ${TASK_DIR}/${JAR_FILE} --spring.profiles.active=prod >/dev/null 2>&1 &
