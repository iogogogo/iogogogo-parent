package com.github.iogogogo.task.domain.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by tao.zeng on 2021/2/27.
 */
@Data
public class JobDTO implements Serializable {

    private String jobName;

    private String jobGroupName;

    private String cron;

    private String description;
}
