package com.github.iogogogo.task.manager;

import com.github.iogogogo.task.domain.IJobTask;
import lombok.Data;
import org.quartz.JobDataMap;
import org.quartz.SchedulerException;

import java.io.Serializable;
import java.util.List;

/**
 * The interface Task service.
 */
public interface TaskService {

    /**
     * The constant DEFAULT_TASK_GROUP.
     */
    String DEFAULT_TASK_GROUP = "quartz-common-group";

    /**
     * Add job.
     *
     * @param jobClass     the job class
     * @param jobId        the job id
     * @param jobGroupName the job group name
     * @param jobTime      the job time
     * @param jobTimes     the job times
     * @param jobData      the job data
     * @throws SchedulerException the scheduler exception
     */
    void addJob(Class<? extends IJobTask> jobClass, String jobId,
                String jobGroupName, int jobTime,
                int jobTimes, JobDataMap jobData) throws SchedulerException;


    /**
     * Add job.
     *
     * @param jobClass     the job class
     * @param jobId        the job id
     * @param jobGroupName the job group name
     * @param cron         the cron
     * @param jobData      the job data
     * @throws SchedulerException the scheduler exception
     */
    void addJob(Class<? extends IJobTask> jobClass, String jobId,
                String jobGroupName, String cron, JobDataMap jobData) throws SchedulerException;

    /**
     * Update job.
     *
     * @param jobId        the job id
     * @param jobGroupName the job group name
     * @param jobTime      the job time
     * @throws SchedulerException the scheduler exception
     */
    void updateJob(String jobId, String jobGroupName, String jobTime) throws SchedulerException;

    /**
     * Delete job.
     *
     * @param jobId        the job id
     * @param jobGroupName the job group name
     * @throws SchedulerException the scheduler exception
     */
    void deleteJob(String jobId, String jobGroupName) throws SchedulerException;

    /**
     * Pause job.
     *
     * @param jobId        the job id
     * @param jobGroupName the job group name
     * @throws SchedulerException the scheduler exception
     */
    void pauseJob(String jobId, String jobGroupName) throws SchedulerException;

    /**
     * Resume job.
     *
     * @param jobId        the job id
     * @param jobGroupName the job group name
     * @throws SchedulerException the scheduler exception
     */
    void resumeJob(String jobId, String jobGroupName) throws SchedulerException;

    /**
     * Run a job now.
     *
     * @param jobId        the job id
     * @param jobGroupName the job group name
     * @throws SchedulerException the scheduler exception
     */
    void trigger(String jobId, String jobGroupName) throws SchedulerException;

    /**
     * List job list.
     *
     * @return the list
     * @throws SchedulerException the scheduler exception
     */
    List<JobInfo> listJob() throws SchedulerException;

    /**
     * List running job list.
     *
     * @return the list
     * @throws SchedulerException the scheduler exception
     */
    List<JobInfo> listRunningJob() throws SchedulerException;

    /**
     * Job job info.
     *
     * @param jobId the job id
     * @return the job info
     * @throws SchedulerException the scheduler exception
     */
    JobInfo job(String jobId) throws SchedulerException;

    /**
     * Exists boolean.
     *
     * @param jobId        the job id
     * @param jobGroupName the job group name
     * @return the boolean
     * @throws SchedulerException the scheduler exception
     */
    boolean exists(String jobId, String jobGroupName) throws SchedulerException;


    /**
     * The type Job info.
     */
    @Data
    class JobInfo implements Serializable {

        /**
         * The Job id.
         */
        private String jobId;

        /**
         * The Job group name.
         */
        private String jobGroupName;

        /**
         * The Description.
         */
        private String description;

        /**
         * The Job status.
         */
        private String jobStatus;

        /**
         * The Job time.
         */
        private String jobTime;
    }
}

