package com.github.iogogogo.task.manager.schedule;

import org.springframework.util.StringUtils;

import java.util.Objects;

/**
 * Created by tao.zeng on 2021/3/29.
 */
public class ScheduledTaskBean implements Runnable {

    private final String beanName;

    private final String methodName;

    private final String param;

    /**
     * Instantiates a new Scheduling runnable.
     */
    public ScheduledTaskBean(String beanName, String methodName) {
        this(beanName, methodName, null);
    }

    /**
     * Instantiates a new Scheduling runnable.
     *
     * @param beanName   the bean name
     * @param methodName the method name
     * @param param      the param
     */
    public ScheduledTaskBean(String beanName, String methodName, String param) {
        this.beanName = beanName;
        this.methodName = methodName;
        this.param = param;
    }

    @Override
    public void run() {
        ScheduledTaskUtils.trigger(beanName, methodName, param);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScheduledTaskBean that = (ScheduledTaskBean) o;

        return StringUtils.hasLength(param) ?
                beanName.equals(that.beanName) && methodName.equals(that.methodName) && param.equals(that.param) :
                beanName.equals(that.beanName) && methodName.equals(that.methodName) && that.param == null;
    }

    @Override
    public int hashCode() {
        return StringUtils.hasLength(param) ? Objects.hash(beanName, methodName, param) : Objects.hash(beanName, methodName);
    }
}
