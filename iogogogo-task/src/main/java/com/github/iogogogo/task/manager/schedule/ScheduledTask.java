package com.github.iogogogo.task.manager.schedule;

import java.util.concurrent.ScheduledFuture;

/**
 * Created by tao.zeng on 2021/3/29.
 */
public class ScheduledTask {

    /**
     * The Future.
     */
    volatile ScheduledFuture<?> future;

    /**
     * 取消定时任务
     */
    public void cancel() {
        ScheduledFuture<?> future = this.future;
        if (future != null) {
            future.cancel(true);
        }
    }
}
