package com.github.iogogogo.task.domain;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.lang.NonNull;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * Created by tao.zeng on 2021/2/27.
 */
public abstract class IJobTask extends QuartzJobBean {

    abstract public void scheduled(JobExecutionContext jobExecutionContext) throws JobExecutionException;

    public String getComponentName() {
        return this.getClass().getName();
    }

    @Override
    protected void executeInternal(@NonNull JobExecutionContext jobExecutionContext) throws JobExecutionException {
        scheduled(jobExecutionContext);
    }
}
