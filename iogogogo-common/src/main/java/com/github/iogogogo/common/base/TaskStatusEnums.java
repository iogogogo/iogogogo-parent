package com.github.iogogogo.common.base;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by tao.zeng on 2021/3/30.
 */
@Getter
@AllArgsConstructor
public enum TaskStatusEnums {

    START_SUCCESS(1, "启动"),
    START_FAIL(2, "启动失败"),
    STOP_SUCCESS(-1, "停止"),
    STOP_FAIL(-2, "停止失败"),
    DELETE(0, "删除"),
    ;

    private final int status;

    private final String describe;
}
