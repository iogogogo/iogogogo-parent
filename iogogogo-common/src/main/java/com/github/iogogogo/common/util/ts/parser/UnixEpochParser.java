
package com.github.iogogogo.common.util.ts.parser;

import com.github.iogogogo.common.util.Java8DateTimeUtils;
import com.github.iogogogo.common.util.ts.TimestampParser;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Created by tao.zeng on 2021/3/29.
 */
public class UnixEpochParser implements TimestampParser {

    private final static long MAX_EPOCH_SECONDS = Integer.MAX_VALUE;

    private final long value;

    public UnixEpochParser(long value) {
        this.value = value;
    }

    @Override
    public LocalDateTime parse(List<DateTimeFormatter> pattern) {
        if (value > MAX_EPOCH_SECONDS) {
            throw new IllegalArgumentException("Cannot parse date for value larger than UNIX epoch maximum seconds");
        }
        Instant instant = Java8DateTimeUtils.ofEpochSecond(value);
        return Java8DateTimeUtils.toJava8(instant);
    }
}
