package com.github.iogogogo.common.util.ts;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Created by tao.zeng on 2021/3/26.
 */
public interface TimestampParser {

    /**
     * The constant PATTERN_LIST.
     */
    List<String> PATTERN_LIST = StringUtils.isNotEmpty(System.getProperty("iogogogo.ts.parse.pattern", "")) ?
            Lists.newArrayList(StringUtils.split(System.getProperty("iogogogo.ts.parse.pattern", String.join("##", defaultPattern())), "##")) :
            defaultPattern();

    /**
     * Default pattern list.
     *
     * @return the list
     */
    static List<String> defaultPattern() {
        return Lists.newArrayList(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", // 2020-08-13T08:59:59.904Z
                "yyyy-MM-dd HH:mm:ss:SSSSSS", // 2020-08-13 08:59:59:904001
                "yyyy-MM-dd'T'HH:mm:ss.SSSXXX", // 2020-08-13T08:59:59.904+08:00
                "yyyy-MM-dd HH:mm:ss:SSSSSS", // 2020-08-13 08:59:59:904001
                "yyyy-MM-dd HH:mm:ss", // 2020-08-13 08:59:59
                "yyyy-MM-dd HH:mm:ss.SSSZ",
                "yyyy-MM-dd HH:mm:ss.SSS",
                "yyyy-MM-dd HH:mm:ss,SSSZ",
                "yyyy-MM-dd HH:mm:ss,SSS"
        );
    }

    /**
     * Parse local date time.
     *
     * @param pattern the pattern
     * @return the local date time
     */
    LocalDateTime parse(List<DateTimeFormatter> pattern);
}
