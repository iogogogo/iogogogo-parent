package com.github.iogogogo.common.base.domain;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by tao.zeng on 2021/3/3.
 */
@Data
@Builder
public class ResponseWrapper<T> implements Serializable {

    private int code;

    private String message;

    public T data;
}
