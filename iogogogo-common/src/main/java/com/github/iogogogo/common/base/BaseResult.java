package com.github.iogogogo.common.base;

import com.github.iogogogo.common.base.domain.ResponseWrapper;
import org.springframework.http.HttpStatus;

/**
 * Created by tao.zeng on 2021/3/3.
 */
public interface BaseResult {

    default ResponseWrapper<?> result(int code, String message, Object data) {
        return ResponseWrapper.builder()
                .code(code).message(message).data(data)
                .build();
    }

    default ResponseWrapper<?> ok() {
        return ok(null);
    }

    default ResponseWrapper<?> ok(Object data) {
        return result(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), data);
    }


    default ResponseWrapper<?> failure() {
        return this.failure(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
    }

    default ResponseWrapper<?> failure(String message) {
        return this.failure(HttpStatus.INTERNAL_SERVER_ERROR.value(), message);
    }

    default ResponseWrapper<?> failure(int code, String message) {
        return this.result(code, message, null);
    }
}
