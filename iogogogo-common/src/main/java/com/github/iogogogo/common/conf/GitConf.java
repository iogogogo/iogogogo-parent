package com.github.iogogogo.common.conf;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by tao.zeng on 2021/3/17.
 */
@Data
@Component
@PropertySource("classpath:git.properties")
public class GitConf {

    @Value("${git.commit.id}")
    private String commitId;

    @Value("${git.branch}")
    private String branch;

    @Value("${git.closest.tag.name}")
    private String tagName;

    @Value("${git.build.version}")
    private String buildVersion;

    @Value("${git.commit.time}")
    private String commitTime;

    @Value("${git.build.time}")
    private String buildTime;

    @Value("${git.commit.message.full}")
    private String commitMessageFull;

    @Value("${git.commit.message.short}")
    private String commitMessageShort;

    @Value("${git.build.user.name}")
    private String buildUser;

    @Value("${git.build.host}")
    private String buildHost;

    @Value("${git.commit.user.name}")
    private String commitUser;

    @Value("${git.remote.origin.url}")
    private String remoteUrl;
}
