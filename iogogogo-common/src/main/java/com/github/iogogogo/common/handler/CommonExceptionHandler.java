package com.github.iogogogo.common.handler;

import com.github.iogogogo.common.base.BaseResult;
import com.github.iogogogo.common.exception.CommonException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Created by tao.zeng on 2021/3/29.
 */
@Slf4j
@ResponseBody
@ControllerAdvice
@RestControllerAdvice
public class CommonExceptionHandler implements BaseResult {

    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Object exceptionHandler(Exception exception) {
        log.error("fetch exception handler", exception);
        if (exception instanceof CommonException) {
            CommonException e = (CommonException) exception;
            return result(e.getCode(), e.getMessage(), null);
        }
        return result(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getMessage(), null);
    }
}
