package com.github.iogogogo.common.util;

import java.util.UUID;

/**
 * Created by tao.zeng on 2021/3/30.
 */
public class IdHelper {

    private static final Snowflake SNOWFLAKE = new Snowflake(0);

    public static long id() {
        return SNOWFLAKE.next();
    }

    public static String uuid() {
        return UUID.randomUUID().toString().toLowerCase().replace("-", "");
    }
}
