package com.github.iogogogo.common.util.ts;

import com.github.iogogogo.common.util.ts.parser.LocalDateTimeParser;
import com.github.iogogogo.common.util.ts.parser.UnixEpochParser;
import com.github.iogogogo.common.util.ts.parser.UnixMillisEpochParser;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * Created by tao.zeng on 2021/3/26.
 */
public class TimestampParserFactory {

    private final static int UNIX_LENGTH = 10, UNIX_MS_LENGTH = 13;

    /**
     * Make parser timestamp parser.
     *
     * @param value the value
     * @return the timestamp parser
     */
    public static TimestampParser makeParser(String value) {
        if (NumberUtils.isCreatable(value)) {
            long longValue = NumberUtils.createNumber(value).longValue();
            int length = Long.toString(longValue).length();

            if (length == UNIX_LENGTH) {
                return new UnixEpochParser(longValue);
            } else if (length == UNIX_MS_LENGTH) {
                return new UnixMillisEpochParser(longValue);
            }
        }
        return new LocalDateTimeParser(value);
    }
}
