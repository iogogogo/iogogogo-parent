package com.github.iogogogo.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by tao.zeng on 2021/3/29.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CommonException extends RuntimeException {

    private int code;

    public CommonException(int code, String message) {
        super(message);
        this.code = code;
    }

    public CommonException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public CommonException(int code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public CommonException(int code, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.code = code;
    }
}
