package com.github.iogogogo.aspect;

import com.github.iogogogo.service.SysTaskService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.NonNull;

/**
 * Created by tao.zeng on 2021/3/30.
 * <p>
 * https://iogogogo.gitee.io/2020/02/16/spring-aop/
 */
@Slf4j
@Aspect
@Configuration
public class TaskAspect {

    @Autowired
    private SysTaskService taskService;

    /**
     * Scheduled task.
     */
    @Pointcut("execution(public * com.github.iogogogo.common.task.*Service*.*(..))")
    public void scheduledTask() {
    }

    /**
     * After returning object.
     * <p>
     * 后置通知，可以对任务进行切面处理
     *
     * @param joinPoint the join point
     * @param returnVal the return val
     */
    @AfterReturning(value = "scheduledTask()", returning = "returnVal")
    public void afterReturning(@NonNull JoinPoint joinPoint, Object returnVal) {

        Signature signature = joinPoint.getSignature();

        log.info("execute afterReturning {}", String.join(".", signature.getDeclaringTypeName(), signature.getName()));

        log.info("returnVal:{}", returnVal);
    }
}
