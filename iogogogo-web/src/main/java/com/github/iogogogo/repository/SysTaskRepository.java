package com.github.iogogogo.repository;

import com.github.iogogogo.entity.SysTaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by tao.zeng on 2021/3/30.
 */
@Repository
public interface SysTaskRepository extends JpaRepository<SysTaskEntity, Long> {
}
