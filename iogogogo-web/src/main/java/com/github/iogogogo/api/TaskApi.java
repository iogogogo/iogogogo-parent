package com.github.iogogogo.api;

import com.github.iogogogo.common.base.domain.ResponseWrapper;
import com.github.iogogogo.core.base.BaseApi;
import com.github.iogogogo.domain.dto.TaskDTO;
import com.github.iogogogo.service.SysTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by tao.zeng on 2021/3/29.
 */
@RequestMapping("/api/task")
@RestController
public class TaskApi extends BaseApi {

    @Autowired
    private SysTaskService taskService;

    @GetMapping
    public ResponseWrapper<?> tasks() {
        return ok(taskService.findAll());
    }

    @GetMapping("/{taskId}")
    public ResponseWrapper<?> task(@PathVariable("taskId") long taskId) {
        return ok(taskService.findById(taskId).orElse(null));
    }

    @PostMapping
    public ResponseWrapper<?> create(@RequestBody TaskDTO taskDTO) {
        return ok(taskService.saveTask(taskDTO));
    }

    @PutMapping
    public ResponseWrapper<?> update(@RequestBody TaskDTO taskDTO) {
        return ok(taskService.updateTask(taskDTO));
    }

    @DeleteMapping("/{taskId}")
    public ResponseWrapper<?> delete(@PathVariable("taskId") long taskId) {
        taskService.delete(taskId);
        return ok();
    }

    @GetMapping("/status/{taskId}")
    public ResponseWrapper<?> status(@PathVariable("taskId") long taskId, int status) {
        return ok(taskService.status(taskId, status));
    }

    @GetMapping("/trigger/{taskId}")
    public ResponseWrapper<?> trigger(@PathVariable("taskId") long taskId) {
        return ok(taskService.trigger(taskId));
    }
}
