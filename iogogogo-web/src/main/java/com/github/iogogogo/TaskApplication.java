package com.github.iogogogo;

import com.github.iogogogo.common.task.ScheduledTaskBean;
import com.github.iogogogo.common.task.ScheduledTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by tao.zeng on 2021/3/29.
 */
@Slf4j
@SpringBootApplication
public class TaskApplication implements CommandLineRunner {

    @Autowired
    private ScheduledTaskService scheduledTaskService;

    public static void main(String[] args) {
        SpringApplication.run(TaskApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("TaskApplication start completed.");
        ScheduledTaskBean taskBean = new ScheduledTaskBean("simpleTask", "taskWithParam", "hello world");
        scheduledTaskService.addCronTask(taskBean, "0/5 * * * * *");
    }
}
