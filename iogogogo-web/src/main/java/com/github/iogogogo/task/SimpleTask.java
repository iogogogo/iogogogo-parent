package com.github.iogogogo.task;

import com.github.iogogogo.common.util.Java8DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Created by tao.zeng on 2021/3/29.
 */
@Slf4j
@Component("simpleTask")
public class SimpleTask {

    public void taskWithParam(String param) {
        log.info("taskWithParam:{} {}", Java8DateTimeUtils.nowDateTime(), param);
    }

    public void taskNoParam() {
        log.info("taskNoParam:{}", Java8DateTimeUtils.nowDateTime());
    }
}
