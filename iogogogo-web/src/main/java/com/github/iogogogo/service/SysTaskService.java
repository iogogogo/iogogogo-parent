package com.github.iogogogo.service;

import com.github.iogogogo.domain.dto.TaskDTO;
import com.github.iogogogo.entity.SysTaskEntity;

import java.util.List;
import java.util.Optional;

/**
 * Created by tao.zeng on 2021/3/30.
 */
public interface SysTaskService {

    List<SysTaskEntity> findAll();

    SysTaskEntity saveTask(TaskDTO taskDTO);

    SysTaskEntity updateTask(TaskDTO taskDTO);

    Optional<SysTaskEntity> findById(Long taskId);

    SysTaskEntity status(Long taskId, int status);

    void delete(Long taskId);

    SysTaskEntity trigger(Long taskId);

    interface TaskAction {
        int TASK_START = 1, TASK_STOP = 0;
    }
}
