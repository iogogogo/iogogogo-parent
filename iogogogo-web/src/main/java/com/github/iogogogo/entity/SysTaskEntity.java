package com.github.iogogogo.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by tao.zeng on 2021/3/30.
 */
@Data
@Entity
@Table(name = "sys_task")
public class SysTaskEntity implements Serializable {

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 任务名称
     */
    @Column(name = "name", nullable = false)
    private String name;

    /**
     * 执行的bean名称
     */
    @Column(name = "bean_name", nullable = false)
    private String beanName;

    /**
     * 执行的方法名称
     */
    @Column(name = "method_name", nullable = false)
    private String methodName;

    /**
     * cron表达式
     */
    @Column(name = "cron", nullable = false)
    private String cron;

    /**
     * 任务参数
     */
    @Column(name = "param")
    private String param;

    /**
     * 任务描述信息
     */
    @Column(name = "description")
    private String description;

    /**
     * @see com.github.iogogogo.common.base.TaskStatusEnums
     * <p>
     * 任务状态，1=>启动、2=>启动失败、-1=>停止、-2=>停止失败、0=>删除
     */
    @Column(name = "status", nullable = false)
    private int status;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private LocalDateTime updateTime;

}
