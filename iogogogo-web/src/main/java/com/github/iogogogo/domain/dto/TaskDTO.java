package com.github.iogogogo.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by tao.zeng on 2021/3/30.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskDTO implements Serializable {

    private Long id;

    private String name;

    private String beanName, methodName, cron, param, description;

    public TaskDTO(String name, String beanName, String methodName, String cron, String param, String description) {
        this.name = name;
        this.beanName = beanName;
        this.methodName = methodName;
        this.cron = cron;
        this.param = param;
        this.description = description;
    }
}
