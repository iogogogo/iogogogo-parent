package com.github.iogogogo.kafka.configure;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.AdminClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by tao.zeng on 2021/3/1.
 */
@Slf4j
@Configuration
public class KafkaConfiguration {

    @Autowired
    private KafkaProperties kafkaProperties;

    @Bean(destroyMethod = "close")
    public AdminClient adminClient() {
        return AdminClient.create(kafkaProperties.buildAdminProperties());
    }
}
