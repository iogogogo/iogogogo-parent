package com.github.iogogogo.kafka.manager.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Comparator;

/**
 * Created by tao.zeng on 2021/3/31.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class KafkaData<T> implements Serializable, Comparator<KafkaData<T>> {

    private LocalDateTime dateTime;

    private T data;

    /**
     * int compare(T o1, T o2) 是“比较o1和o2的大小”。
     * 返回“负数”，意味着“o1比o2小”；
     * 返回“零”，意味着“o1等于o2”；
     * 返回“正数”，意味着“o1大于o2”。
     *
     * @param o1 the o1
     * @param o2 the o2
     * @return the int
     */
    @Override
    public int compare(KafkaData o1, KafkaData o2) {
        if (o1.getDateTime().isBefore(o2.getDateTime())) {
            return 1;
        } else if (o1.getDateTime().equals(o2.getDateTime())) {
            return 0;
        } else {
            return -1;
        }
    }
}
