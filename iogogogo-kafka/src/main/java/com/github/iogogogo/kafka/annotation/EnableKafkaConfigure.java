package com.github.iogogogo.kafka.annotation;

import com.github.iogogogo.kafka.conf.KafkaConf;
import com.github.iogogogo.kafka.configure.KafkaConfiguration;
import com.github.iogogogo.kafka.manager.KafkaService;
import com.github.iogogogo.kafka.manager.impl.KafkaServiceImpl;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Created by tao.zeng on 2021/3/31.
 */
@Inherited
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import({KafkaConf.class, KafkaConfiguration.class, KafkaService.class, KafkaServiceImpl.class})
public @interface EnableKafkaConfigure {
}
