package com.github.iogogogo.kafka.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Created by tao.zeng on 2021/3/1.
 */
@Component
public class KafkaConf {

    @Value("${kafka.future.timeout.interval:1}")
    private long timeout;

    @Value("${kafka.future.timeout.unit:MINUTES}")
    private String unit;

    @Value("${kafka.max.partition.fetch.bytes:2048}")
    private long maxFetchSize;

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public TimeUnit getUnit() {
        switch (unit.toUpperCase()) {
            case "SECONDS":
                return TimeUnit.SECONDS;
            case "MS":
                return TimeUnit.MILLISECONDS;
            case "NS":
                return TimeUnit.NANOSECONDS;
            case "HOURS":
                return TimeUnit.HOURS;
            case "DAYS":
                return TimeUnit.DAYS;
            default:
                return TimeUnit.MINUTES;
        }
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public long getMaxFetchSize() {
        return maxFetchSize;
    }

    public void setMaxFetchSize(long maxFetchSize) {
        this.maxFetchSize = maxFetchSize;
    }
}
